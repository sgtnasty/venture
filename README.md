# venture

Random adventures, loot, encounters and epic gear.

## Setup

    git clone ...

Setup python3 virtual environment.

https://realpython.com/python-virtual-environments-a-primer/

    python3 -m venv env

Activate venv

    source ./env/bin/activate

Install dependencies

    pip install -r requirements.txt

One command to rule them all:

    virtualenv ./env && source ./env/bin/activate && pip install -r requirements.txt

Deactivate

    deactivate

## Dev

Update dependencies:

    pip3 freeze > requirements.txt

    pip3 install -r requirements.txt

## Running

Change to the venture repo directory and run:

    python3 -m venture

