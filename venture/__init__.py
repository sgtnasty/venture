
import logging
import numpy.random


VERSION = (1, 0, 1)
__version__ = "".join([".".join(map(str, VERSION[0:3])), "".join(VERSION[3:])])
NAME = "Venture"


def roll1d6():
    v = numpy.random.randint(1, high=6)
    return v


def roll3d6():
    a = roll1d6()
    b = roll1d6()
    c = roll1d6()
    v = a + b + c
    return (a, b, c, v)


def roll1d100():
    v = numpy.random.randint(1, high=100)
    return v


def rollnd100(n):
    v = 0
    for i in range(n):
        v = v + roll1d100()
    return v


FMT = "%(asctime)s - %(module)s.%(funcName)s[%(process)d] - %(levelname)s - %(message)s"


class Session(object):
    """docstring for Session"""
    def __init__(self, name):
        super(Session, self).__init__()
        self.log = logging.getLogger(self.__class__.__name__)
        logging.basicConfig(level=logging.DEBUG, format=FMT)
        self.name = name
        self.p = numpy.random.choice([1, 2, 3, 4, 5, 6])

    def __repr__(self):
        return "<{}.{}: arg={}, p={}>".format(
            __name__, self.__class__.__name__, self.name, self.p)

    def run(self):
        self.log.info("Session started")
        self.log.debug("{}".format(repr(self)))
        self.log.info("Session completed")
