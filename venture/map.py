
import math


class Point(object):
    """docstring for Point"""
    def __init__(self, x, y):
        super(Point, self).__init__()
        self.x = x
        self.y = y

    def __repr__(self):
        return("x={}, y={}".format(self.x, self.y))

    def distance_to(self, target):
        deltax = target.x - self.x
        deltay = target.y - self.y
        sqx - math.pow(deltax, 2)
        sqy = math.pow(deltay, 2)
        q = sqx + sqy
        return math.sqrt(q)


class Location(object):
    """docstring for Location"""
    def __init__(self, loc):
        super(Location, self).__init__()
        self.loc = loc
        self.start_location = Point(self.loc.x, self.loc.y)

    def move(self, x, y):
        self.locX = self.locX + x
        self.locY = self.locY + y

    def __repr__(self):
        return "x={}, y={}: (started: {})".format(
            repr(self.loc),
            self.loc.distance_to(self.started)
