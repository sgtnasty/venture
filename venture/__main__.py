
import venture


class Player(object):
    """docstring for Player"""
    def __init__(self, name):
        super(Player, self).__init__()
        self.name = name

    def __str__(self):
        return "{}: {}".format(self.__class__.__name__, self.name)


if __name__ == '__main__':
    print("{} v{}".format(venture.NAME, venture.__version__))
    print("roll 1d6: {}".format(venture.roll1d6()))
    print("roll 1d100: {}".format(venture.roll1d100()))
    print("roll 6 x d100: {}".format(venture.rollnd100(6)))
    p1 = Player("Fred")
    print(p1)
    STR = venture.roll3d6()
    DEX = venture.roll3d6()
    CON = venture.roll3d6()
    INT = venture.roll3d6()
    WIS = venture.roll3d6()
    CHA = venture.roll3d6()
    print("STR({}+{}+{}) = {}".format(STR[0], STR[1], STR[2], STR[3]))
    print("DEX({}+{}+{}) = {}".format(DEX[0], DEX[1], DEX[2], DEX[3]))
    print("CON({}+{}+{}) = {}".format(CON[0], CON[1], CON[2], CON[3]))
    print("INT({}+{}+{}) = {}".format(INT[0], INT[1], INT[2], INT[3]))
    print("WIS({}+{}+{}) = {}".format(WIS[0], WIS[1], WIS[2], WIS[3]))
    print("CHA({}+{}+{}) = {}".format(CHA[0], CHA[1], CHA[2], CHA[3]))
    session = venture.Session("venture")
    session.run()
